﻿#include <iostream>
#include <vector>
#include <string>
using namespace std;

template <typename T>
class Stack
{
private:
    T* stack = nullptr;
    int length = 0;
    int capacity = 0;

    void ensureCapacity()
    {
        if (length <= capacity)
        {
            return;
        }

        T* newStack = new T[length];

        for (int i = 0; i < capacity; ++i)
        {
            newStack[i] = stack[i];
        }

        capacity = length;

        if (stack != nullptr)
        {
            delete stack;
        }
        stack = newStack;
    }

public:
    Stack()
    {}

    void push(T item)
    {
        ++length;
        ensureCapacity();
        stack[length - 1] = item;
    }

    T pop()
    {
        if (length == 0) {
            cout << "Ne zdan massiv";
            push(0);
        }
        length--;
        return stack[length];
    }

};

int main()
{
    Stack<int> stack;

   /* stack.push(10);
    stack.push(12);
    stack.push(15);*/

    /*cout << stack.pop() << ' ';
    cout << stack.pop() << ' ';*/

    //stack.push(20);

    //cout << stack.pop() << ' ';
    cout << stack.pop() << '\n';
}
